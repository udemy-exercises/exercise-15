﻿using System;
using System.Collections.Generic;

class Program
{
   static void Main()
   {
      string[] inputFirst = Console.ReadLine().Split(' ');
      int M = int.Parse(inputFirst[0]);
      int N = int.Parse(inputFirst[1]);

      int[,] mat = new int[M, N];
      for (int i = 0; i < M; i++)
      {
         string[] input = Console.ReadLine().Split();

         for (int j = 0; j < N; j++) {
            mat[i, j] = int.Parse(input[j]);
         }
      }

      Console.Clear();

      for (int i = 0; i < M; i++)
      {
         for (int j = 0; j < N; j++) {
            Console.Write($"{mat[i, j]} ");
         }

         Console.WriteLine();
      }

      Console.Write("\nType in a number to look in the array: ");
      int number = int.Parse(Console.ReadLine());

      List<int> list = new List<int>();
      int countNumber = 0;

      for (int i = 0; i < M; i++)
      {
         for (int j = 0; j < N; j++) {
            if (mat[i, j] == number) {
               list.Insert(countNumber, i);
               countNumber++;
               list.Insert(countNumber, j);
               countNumber++;
            }
         }
      }

      Console.WriteLine();
      for (int i = 0; i < countNumber -1; i += 2)
      {
         Console.WriteLine($"Position {list[i]}, {list[i +1]}");

         int left = list[i+1] -1;
         int right = list[i+1] +1;
         int up = list[i] -1;
         int down = list[i] +1;

         if (left > -1) {
            Console.WriteLine($"Left {mat[list[i], left]}");
         }

         if (right < N) {
            Console.WriteLine($"Right {mat[list[i], right]}");
         }

         if (up > -1) {
            Console.WriteLine($"Up {mat[up, list[i+1]]}");
         }

         if (down < M) {
            Console.WriteLine($"Down {mat[down, list[i+1]]}");
         }
      }
   }
}